#include "lista.h"
#include <stdio.h>
#include <stdlib.h>

// ----> Listas enlazadas de tipo entero
// Creación de una lista 
nodo *crearListaINT(nodo *lista){
    //Incializando una lista vacía
    return lista = NULL;
}
// Método para insertar un elemento de tipo nodo al inicio de la lista
nodo *insertarNodoInicioINT(int valor, nodo *lista){
    //Declaracion de una variable de tipo nodo --> *nodoNuevo
    nodo *nodoNuevo;
    //La función malloc reservar un espacio de memoria del tamaño del nodo
    //La función sizeof determina el tamaño de un tipo de dato, en este caso el tipo de dato "nodo"
    //Por lo tanto se reserva un espacio de memoria del tamaño necesario para almacenar un nodo
    //El espacio de memoria se almacena en nodo Nuevo
    nodoNuevo = (nodo *)malloc(sizeof(nodo));
    //Si nodoNuevo es diferente de NULL significa que el espacio de memoria se reservo correctamente
    if(nodoNuevo != NULL){
        nodoNuevo -> dato = valor;
        nodoNuevo -> siguiente = lista;
        lista = nodoNuevo;
    }
    //Retormanos lista
    return lista;
}

// Método para insertar un elemento de tipo nodo al final de la lista
nodo *insertarNodoFinalINT(int valor, nodo *lista){
    //Declaramos *nodoNuevo y *nodoAuxiliar
    nodo *nodoNuevo, *nodoAuxiliar;
    //Se reserva el espacio de memoria
    nodoNuevo = (nodo *) malloc(sizeof(nodo));
    //Si nodoNuevo es diferente de NULL significa que el espacio de memoria se reservo correctamente
    if(nodoNuevo != NULL){
        //Asignamos el valor que se paso como parametro a nodoNuevo
        nodoNuevo -> dato = valor;
        nodoNuevo -> siguiente = NULL;
        //Si la lista esta vacía
        if(lista==NULL){
            //Se agrega al incio
            lista=nodoNuevo;
        }else{
            //De no ser que este vacía
            nodoAuxiliar = lista;
            //Se ejecutara miestras el nodoAuxiliar en su campo siguiente se NULL
            while (nodoAuxiliar -> siguiente != NULL){
                //Se recorrera de nodo en nodo
                nodoAuxiliar = nodoAuxiliar -> siguiente;
            }
            //Insertamos nodoNuevo
            nodoAuxiliar -> siguiente = nodoNuevo;
        }
    }
    //Retormanos lista
    return lista;
}

// Método para imprimir los elementos de la lista
void imprimirListaINT(nodo *lista){
    //Declaración de un nodoAuxiliar
    nodo *nodoAuxiliar;
    //nodoAuxiliar apunta a lista
    nodoAuxiliar = lista;
    printf("Inicio INT -> ");
    //Se ejecutara mientras nodoAuxiliar sea diferente de NULL
    while (nodoAuxiliar != NULL){
        //Se imprime el nodo
        printf("%d -> ", nodoAuxiliar -> dato);
        //nodoAuxuliar apunta al siguiente nodo
        nodoAuxiliar = nodoAuxiliar -> siguiente;
    }
    printf("NULL\n");
}

// Método para retirar un elemento al inicio de la lista
nodo *retirarInicioINT(nodo *lista){
    //Creacion de un nodoAuxiliar
    nodo *nodoAuxiliar;
    //Creación de una variable de tipo int para almacenar el elemento a eliminar
    int dato;
    //Comprobación de que la lista NO este vacía
    if(lista == NULL){
        printf("--ERROR--\nNo existen elementos en la lista INT");
    }else{
        //Buscar el inicio de la lista
        nodoAuxiliar = lista;
        dato = nodoAuxiliar -> dato;
        lista = nodoAuxiliar -> siguiente;
        printf("Se ha eliminado el elemento %d del inicio\n", dato);
        //Liberación del espacio de memoria
        free(nodoAuxiliar);
    }
    //Retormanos lista
    return lista;
}

// Método para retirar un elemnto al final de la lista
nodo *retirarFinalINT(nodo *lista){
    //Creacion de un nodoAuxiliar  y nodoAnterior
    nodo *nodoAuxiliar, *nodoAnterior;
    //Creación de una variable de tipo int para almacenar el elemento a eliminar
    int dato;
    //Comprobación de que la lista NO este vacía
    if(lista == NULL){
        printf("--ERROR--\nNo existen elementos en la lista INT");
    }else{
        //Buscar el inicio de la lista
        nodoAuxiliar = lista;
        //Se recorre las lista hasta llegar una posición antes del NULL
        while(nodoAuxiliar -> siguiente != NULL){
            nodoAnterior = nodoAuxiliar;
            nodoAuxiliar = nodoAuxiliar -> siguiente;
        }
        dato = nodoAuxiliar -> dato;
        nodoAnterior -> siguiente = NULL;
        printf("Se ha eliminado el elemento %d del final\n", dato);
        //Liberación del espacio de memoria
        free(nodoAuxiliar);
    }
    //Retormanos lista
    return lista;
}

// ----> Listas enlazadas de tipo float(decimal)
// Creación de una lista
nodoF *crearListaFLOAT(nodoF *listaF){
    //Incializando una lista vacía
    return listaF = NULL;
}

// Método para insertar un elemento de tipo nodo al inicio de la lista
nodoF *insertarNodoInicioFLOAT(float valorF, nodoF *listaF){
    //Declaracion de una variable de tipo nodo --> *nodoNuevo
    nodoF *nodoNuevoF;
    //La función malloc reservar un espacio de memoria del tamaño del nodo
    //La función sizeof determina el tamaño de un tipo de dato, en este caso el tipo de dato "nodo"
    //Por lo tanto se reserva un espacio de memoria del tamaño necesario para almacenar un nodo
    //El espacio de memoria se almacena en nodo Nuevo
    nodoNuevoF = (nodoF *)malloc(sizeof(nodoF));
    //Si nodoNuevo es diferente de NULL significa que el espacio de memoria se reservo correctamente
    if(nodoNuevoF != NULL){
        nodoNuevoF -> datoF = valorF;
        nodoNuevoF -> siguienteF = listaF;

        listaF = nodoNuevoF;
    }
    //Retormanos lista
    return listaF;
}

// Método para insertar un elemento de tipo nodo al final de la lista
nodoF *insertarNodoFinalFLOAT(float valorF, nodoF *listaF){
    //Declaramos *nodoNuevo y *nodoAuxiliar
    nodoF *nodoNuevoF, *nodoAuxiliarF;
    //Se reserva el espacio de memoria
    nodoNuevoF = (nodoF *) malloc(sizeof(nodoF));
    //Si nodoNuevo es diferente de NULL significa que el espacio de memoria se reservo correctamente
    if(nodoNuevoF != NULL){
        //Asignamos el valor que se paso como parametro a nodoNuevo
        nodoNuevoF -> datoF = valorF;
        nodoNuevoF -> siguienteF = NULL;
        //Si la lista esta vacía
        if(listaF==NULL){
            //Se agrega al incio
            listaF=nodoNuevoF;
        }else{
            //De no ser que este vacía
            nodoAuxiliarF = listaF;
            //Se ejecutara miestras el nodoAuxiliar en su campo siguiente se NULL
            while (nodoAuxiliarF -> siguienteF != NULL){
                //Se recorrera de nodo en nodo
                nodoAuxiliarF = nodoAuxiliarF -> siguienteF;
            }
            //Insertamos nodoNuevo
            nodoAuxiliarF -> siguienteF = nodoNuevoF;
        }   
    }
    //Retormanos lista
    return listaF;
}

// Método para imprimir los elementos de la lista
void imprimirListaFLOAT(nodoF *listaF){
    //Declaración de un nodoAuxiliar
    nodoF *nodoAuxiliarF;
    //nodoAuxiliar apunta a lista
    nodoAuxiliarF = listaF;
    printf("Inicio FLOAT -> ");
    //Se ejecutara mientras nodoAuxiliar sea diferente de NULL
    while (nodoAuxiliarF != NULL){
        //Se imprime el nodo
        printf("%.3f -> ", nodoAuxiliarF -> datoF);
        //nodoAuxuliar apunta al siguiente nodo
        nodoAuxiliarF = nodoAuxiliarF -> siguienteF;
    }
    printf("NULL\n");
}

// Método para retirar un elemento al inicio de la lista
nodoF *retirarInicioFLOAT(nodoF *listaF){
    //Creación de un nodoAuxiliar
    nodoF *nodoAuxiliarF;
    //Creación de una variable de tipo float para almacenar el elemento a eliminar
    float datoF;
    //Comprobación de que la lista NO este vacía
    if(listaF == NULL){
        printf("--ERROR--\nNo existen elementos en la lista FLOAT");
    }else{
        //Buscar el inicio de la lista
        nodoAuxiliarF = listaF;
        datoF = nodoAuxiliarF -> datoF;
        listaF = nodoAuxiliarF -> siguienteF;
        printf("Se ha eliminado el elemento %.3f del inicio FLOAT\n", datoF);
        //Liberación del espacio de memoria
        free(nodoAuxiliarF);
    }
    //Retormanos lista
    return listaF;
}

// Método para retirar un elemnto al final de la lista
nodoF *retirarFinalFLOAT(nodoF *listaF){
    //Creacion de un nodoAuxiliar  y nodoAnterior
    nodoF *nodoAuxiliarF, *nodoAnteriorF;
    //Creación de una variable de tipo float para almacenar el elemento a eliminar
    float datoF;
    //Comprobación de que la lista NO este vacía
    if(listaF == NULL){
        printf("--ERROR--\nNo existen elementos en la lista FLOAT");
    }else{
        //Buscar el inicio de la lista
        nodoAuxiliarF = listaF;
        //Se recorre las lista hasta llegar una posición antes del NULL
        while(nodoAuxiliarF -> siguienteF != NULL){
            nodoAnteriorF = nodoAuxiliarF;
            nodoAuxiliarF = nodoAuxiliarF -> siguienteF;
        }
        datoF = nodoAuxiliarF -> datoF;
        nodoAnteriorF -> siguienteF = NULL;
        printf("Se ha eliminado el elemento %.3f del final FLOAT\n", datoF);
        //Liberación del espacio de memoria
        free(nodoAuxiliarF);
    }
    //Retormanos lista
    return listaF;
}
