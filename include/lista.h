/**
 * @file lista.h
 * @version v0.0
 * @brief Cabecera del fichero lista.c
 * @author Castillo Hernández Geovanni
 * @date 2020-04-17
 */
#ifndef LISTA_H
#define LISTA_H

//----------Listas Enlazadas de tipo INT----------
//Definición de la clase Nodo
struct structNodo{
    /**
     * @brief dato --> almacenamiento de los datos de tipo entero
     */
    int dato;
    /**
     * @brief *siguiente --> apuntador al siguiente nodo de la lista
     */
    struct structNodo *siguiente;
};
/**
 * @brief Definicion del tipo de dato Nodo
 */
typedef struct structNodo nodo;
/**
 * @brief Función para crear una lista de tipo entero
 * @param lista Recibe un dato de tipo Nodo que sera un apuntador a la lista de tipo entero
 * @return nodo* --> Retorna un Nodo
 */
nodo *crearListaINT(nodo *lista);
/**
 * @brief Función para insertar un nodo al inicio de la lista de tipo entero
 * @param valor Valor de tipo entero
 * @param lista Un apuntador al inicio de las lista de tipo entero
 * @return nodo* --> Retorna un Nodo
 */
nodo *insertarNodoInicioINT(int valor, nodo *lista);
/**
 * @brief Función para insertar un nodo al final de la lista de tipo entero
 * @param valor Valor de tipo entero
 * @param lista Un apuntador al final de la lista de tipo entero
 * @return nodo* --> Retorna un nodo
 */
nodo *insertarNodoFinalINT(int valor, nodo *lista);
/**
 * @brief Función para imprimir los elementos que se encuentran en la lista de tipo entero
 * @param lista Recibe como parametro un tipo de dato nodo y es un apuntador al inicio de la lista de tipo entero
 */
void imprimirListaINT(nodo *lista);
/**
 * @brief Función de tipo nodo para retirar elementos al inicio de la lista de tipo entero
 * @param lista Recibe una lista como parametro
 * @return nodo* --> Retorna un nodo
 */
nodo *retirarInicioINT(nodo *lista);
/**
 * @brief Función de tipo nodo para retirar elementos al final de la lista de tipo entero
 * @param lista Recibe una lista como parametro
 * @return nodo* --> Retorna un nodo
 */
nodo *retirarFinalINT(nodo *lista);


//----------Listas Enlazadas de tipo Float----------
//Definición de la clase Nodo
struct structNodoF{
    /**
     * @brief dato --> almacenamiento de los datos de tipo float
     */
    float datoF;
    /**
     * @brief *siguiente --> apuntador al siguiente nodo de la lista
     */
    struct structNodoF *siguienteF;
};
/**
 * @brief Definicion del tipo de dato Nodo
 */
typedef struct structNodoF nodoF;
/**
 * @brief Función para crear una lista de tipo float(decimal)
 * @param listaF Recibe un dato de tipo Nodo que sera un apuntador a la lista de tipo float(decimal)
 * @return nodoF* --> Retorna un Nodo
 */
nodoF *crearListaFLOAT(nodoF *listaF);
/**
 * @brief Función para insertar un nodo al inicio de la lista de tipo entero
 * @param valorF Valor de tipo float(decimal)
 * @param listaF Un apuntador al inicio de las lista de tipo float(decimal)
 * @return nodoF* --> Retorna un Nodo
 */
nodoF *insertarNodoInicioFLOAT(float valorF, nodoF *listaF);
/**
 * @brief Función para insertar un nodo al final de la lista de tipo float(decimal)
 * @param valorF Valor de tipo float(decimal)
 * @param listaF Un apuntador al final de la lista de tipo float(decimal)
 * @return nodoF* --> Retorna un nodo
 */
nodoF *insertarNodoFinalFLOAT(float valorF, nodoF *listaF);
/**
 * @brief Función para imprimir los elementos que se encuentran en la lista de tipo float(decimal)
 * @param listaF Recibe como parametro un tipo de dato nodo y es un apuntador al inicio de la lista de tipo float(decimal)
 */
void imprimirListaFLOAT(nodoF *listaF);
/**
 * @brief Función de tipo nodo para retirar elementos al inicio de la lista de tipo float(decimal)
 * @param listaF Recibe una lista como parametro
 * @return nodoF* --> Retorna un nodo
 */
nodoF *retirarInicioFLOAT(nodoF *listaF);
/**
 * @brief Función de tipo nodo para retirar elementos al final de la lista de tipo float(decimal)
 * @param listaF Recibe una lista como parametro
 * @return nodoF* --> Retorna un nodo
 */
nodoF *retirarFinalFLOAT(nodoF *listaF);

#endif //LISTA_H