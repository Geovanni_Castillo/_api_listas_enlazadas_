#CFLAGS = -c

#ejecutable: main.o
#	$(CC) main.c -o ejecutable

#main.o: main.c
#	$(CC) $(CFLAGS) main.c

#clean:
#	$(RM) *.o ejecutable core

CFLAGS = -c

bin/ejecutable: out_make/main.o
	$(CC) out_make/main.o -o bin/ejecutable


out_make/main.o:
	$(CC) $(CFLAGS) -Iinclude src/main.c -o out_make/main.o

clean:
	$(RM) *.o bin/ejecutable core
	$(RM) *.o out_make/main.o core

